/**
 *
 * 日    期：12-2-23
 */
package com.jprass.oap.response;

import java.util.Locale;

import com.jprass.oap.message.MainMessages;
import com.jprass.oap.message.MessageType;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @version 1.0
 */
public class ServiceUnavailableErrorResponse extends OapResponse<String> {

	// 注意，这个不能删除，否则无法进行流化
	public ServiceUnavailableErrorResponse() {
	}

	public ServiceUnavailableErrorResponse(String method, Locale locale, Throwable throwable) {
		OapResponse<String> response = MainMessages.get(MessageType.ISP_SERVICE_UNAVAILABLE, locale);
		this.setHeader(response.getHeader()).setBody(response.getBody());
	}
}
