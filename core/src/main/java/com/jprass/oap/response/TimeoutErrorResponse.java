/**
 * 版权声明： 版权所有 违者必究 2012
 * 日    期：12-6-5
 */
package com.jprass.oap.response;

import java.util.Locale;

import com.jprass.oap.message.MainMessages;
import com.jprass.oap.message.MessageType;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @version 1.0
 */
public class TimeoutErrorResponse extends OapResponse<String> {

	public TimeoutErrorResponse() {
	}

	public TimeoutErrorResponse(String method, Locale locale, int timeout) {
		OapResponse<String> response = MainMessages.get(MessageType.ISP_SERVICE_TIMEOUT, locale);
		this.setBody(response.getBody()).setHeader(response.getHeader());
	}

}
