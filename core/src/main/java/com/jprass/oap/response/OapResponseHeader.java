/**
 * 
 */
package com.jprass.oap.response;

import java.io.Serializable;

public class OapResponseHeader implements Serializable {
	private static final long serialVersionUID = -6671900927701397763L;

	private String code;

	private String message;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
