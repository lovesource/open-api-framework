package com.jprass.oap.response;

public class OapResponse<T extends Object> {

	protected OapResponseHeader header = new OapResponseHeader();

	protected T body;

	public OapResponse() {
	}

	public static OapResponse header(String code) {
		OapResponseHeader header = new OapResponseHeader();
		header.setCode(code);
		return header(header);
	}

	public static OapResponse header(String code, String message) {
		OapResponseHeader header = new OapResponseHeader();
		header.setCode(code);
		header.setMessage(message);
		return header(header);
	}
	
	public static OapResponse bodyHeader(boolean body, String code, String message) {
		OapResponseHeader header = new OapResponseHeader();
		header.setCode(code);
		header.setMessage(message);
		OapResponse response = new OapResponse();
		response.setBody(body);
		response.setHeader(header);
		return response;
	}

	public static OapResponse header(OapResponseHeader header) {
		OapResponse response = new OapResponse();
		response.setHeader(header);
		return response;
	}

	public static OapResponse body(Object body) {
		OapResponse response = new OapResponse();
		response.setBody(body);
		return response;
	}

	public OapResponse<T> setCode(String code) {
		this.header.setCode(code);
		return this;
	}

	public T getBody() {
		return body;
	}

	public OapResponse<T> setBody(T body) {
		this.body = body;
		return this;
	}

	public OapResponseHeader getHeader() {
		return header;
	}

	public OapResponse<T> setHeader(OapResponseHeader header) {
		this.header = header;
		return this;
	}

}
