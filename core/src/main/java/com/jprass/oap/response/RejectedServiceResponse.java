/**
 * 版权声明： 版权所有 违者必究 2012
 * 日    期：12-8-2
 */
package com.jprass.oap.response;

import com.jprass.oap.RopRequestContext;
import com.jprass.oap.message.MainMessages;
import com.jprass.oap.message.MessageType;

/**
 * <pre>
 *   当服务平台资源耗尽（超过最大线程数且列队排满后）
 * </pre>
 *
 * @version 1.0
 */
public class RejectedServiceResponse extends OapResponse<Object> {

	public RejectedServiceResponse(RopRequestContext context) {
		OapResponse<String> response = MainMessages.get(MessageType.FORBIDDEN_REQUEST, context.getLocale(), context.getMethod(), context.getVersion());
		this.setHeader(response.getHeader()).setBody(response.getBody());
	}
}
