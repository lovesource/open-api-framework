package com.jprass.oap.config;

import com.jprass.oap.event.RopEventListener;

public class OapEventListenerHodler {

	private RopEventListener<?> ropEventListener;

	public OapEventListenerHodler(RopEventListener<?> ropEventListener) {
		this.ropEventListener = ropEventListener;
	}

	public RopEventListener<?> getRopEventListener() {
		return ropEventListener;
	}
}
