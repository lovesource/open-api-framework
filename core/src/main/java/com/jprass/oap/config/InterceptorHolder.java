package com.jprass.oap.config;

import com.jprass.oap.Interceptor;

public class InterceptorHolder {

	private Interceptor interceptor;

	public InterceptorHolder(Interceptor interceptor) {
		this.interceptor = interceptor;
	}

	public Interceptor getInterceptor() {
		return interceptor;
	}
}
