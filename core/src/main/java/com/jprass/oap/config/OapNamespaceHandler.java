package com.jprass.oap.config;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class OapNamespaceHandler extends NamespaceHandlerSupport {

	public void init() {
		registerBeanDefinitionParser("annotation-driven", new AnnotationDrivenBeanDefinitionParser());
		registerBeanDefinitionParser("interceptors", new InterceptorsBeanDefinitionParser());
		registerBeanDefinitionParser("listeners", new ListenersBeanDefinitionParser());
		registerBeanDefinitionParser("sysparams", new SystemParameterNamesBeanDefinitionParser());
	}
}
