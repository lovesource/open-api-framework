package com.jprass.oap;

/**
 * <pre>
 *   ROP的异常。
 * </pre>
 *
 */
public class RopException extends RuntimeException {
	private static final long serialVersionUID = 5988622523088194044L;

	public RopException() {
    }

    public RopException(String message) {
        super(message);
    }

    public RopException(String message, Throwable cause) {
        super(message, cause);
    }

    public RopException(Throwable cause) {
        super(cause);
    }
}

