package com.jprass.oap.message;

import com.jprass.oap.response.OapResponse;
import com.jprass.oap.response.OapResponseHeader;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.util.Assert;

import java.util.Locale;

public class MainMessages {

    protected static Logger logger = LoggerFactory.getLogger(MainMessages.class);

    // 错误信息的国际化信息
    private static MessageSourceAccessor messageSourceAccessor;

    private static MessageSourceAccessor solutionSourceAccessor;

    public static OapResponse<String> get(String code, Locale locale, Object... params) {
        String message = getMessage(code, locale, params);

        if (StringUtils.isEmpty(message)) {
            message = "操作失败";
        }

        String solution = getSolution(code, locale);
        if (StringUtils.isEmpty(solution)) {
            solution = "请联系系统管理员";
        }
        OapResponseHeader header = new OapResponseHeader();
        OapResponse<String> response = new OapResponse<String>();
        header.setMessage(message);
        header.setCode(code);
        response.setBody(solution).setHeader(header);
        return response;
    }

    public static OapResponse<String> get(MessageType mainErrorType, Locale locale, Object... params) {
        String message = getMessage(mainErrorType.getCode(), locale, params);

        if (StringUtils.isEmpty(message)) {
            message = "操作失败";
        }

        String solution = getSolution(mainErrorType.getCode(), locale);
        if (StringUtils.isEmpty(solution)) {
            solution = "请联系系统管理员";
        }
        OapResponseHeader header = new OapResponseHeader();
        OapResponse<String> response = new OapResponse<String>();
        header.setMessage(message);
        header.setCode(mainErrorType.getCode());
        response.setBody(solution).setHeader(header);
        return response;
    }

    public static void setMessageSourceAccessor(MessageSourceAccessor messageSourceAccessor) {
        MainMessages.messageSourceAccessor = messageSourceAccessor;
    }

    public static void setSolutionSourceAccessor(MessageSourceAccessor solutionSourceAccessor) {
        MainMessages.solutionSourceAccessor = solutionSourceAccessor;
    }

    public static String getMessage(String code, Locale locale, Object... params) {
        try {
            Assert.notNull(messageSourceAccessor, "请先设置错误消息的国际化资源");
            return messageSourceAccessor.getMessage(code, params, locale);
        } catch (NoSuchMessageException e) {
            logger.error("不存在对应的错误键：，请检查是否在i18n/error的错误资源" + code);
            throw e;
        }
    }

    public static String getSolution(String code, Locale locale) {
        try {
            Assert.notNull(solutionSourceAccessor, "请先设置错误消息的国际化资源");
            return solutionSourceAccessor.getMessage(code, new Object[]{}, locale);
        } catch (NoSuchMessageException e) {
            logger.error("不存在对应的错误键：，请检查是否在i18n/error的错误资源" + code);
            throw e;
        }
    }

}
