package com.jprass.oap.message;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @version 1.0
 */
public enum MessageType {
	SUCCESS("success"),
    SERVICE_CURRENTLY_UNAVAILABLE("service_currently_unavailable"),
    INSUFFICIENT_ISV_PERMISSIONS("insufficient_isv_permissions"),
    INSUFFICIENT_USER_PERMISSIONS("insufficient_user_permissions"),
    UPLOAD_FAIL("upload_fail"),
    HTTP_ACTION_NOT_ALLOWED("http_action_not_allowed"),
    INVALID_ENCODING("invalid_encoding"),
    FORBIDDEN_REQUEST("forbidden_request"),
    METHOD_OBSOLETED("method_obsoleted"),
    BUSINESS_LOGIC_ERROR("business_logic_error"),
    MISSING_SESSION("missing_session"),
    INVALID_SESSION("invalid_session"),
    MISSING_APP_KEY("missing_app_key"),
    INVALID_APP_KEY("invalid_app_key"),
    MISSING_SIGNATURE("missing_signature"),
    INVALID_SIGNATURE("invalid_signature"),
    MISSING_METHOD("missing_method"),
    INVALID_METHOD("invalid_method"),
    MISSING_VERSION("missing_version"),
    INVALID_VERSION("invalid_version"),
    UNSUPPORTED_VERSION("unsupported_version"),
    INVALID_FORMAT("invalid_format"),
    MISSING_REQUIRED_ARGUMENTS("missing_required_arguments"),
    INVALID_ARGUMENTS("invalid_arguments"),
    EXCEED_USER_INVOKE_LIMITED("exceed_user_invoke_limited"),
    EXCEED_SESSION_INVOKE_LIMITED("exceed_session_invoke_limited"),
    EXCEED_APP_INVOKE_LIMITED("exceed_app_invoke_limited"),
    EXCEED_APP_INVOKE_FREQUENCY_LIMITED("exceed_app_invoke_frequency_limited"),
	ISP_SERVICE_UNAVAILABLE("isp.xxx-service-unavailable"),
    ISP_SERVICE_TIMEOUT("isp.xxx-service-timeout"),
    ISV_INVALID_PERMISSION("isv.invalid-permission"),
    ISV_INVALID_PARAMETE("isv.invalid-paramete");
	private String code;
	 
	private MessageType(String code) {
        this.code = code;
    }
	
    public String getCode() {
		return code;
	}
}

