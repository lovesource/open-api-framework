/**
 *
 * 日    期：12-2-27
 */
package com.jprass.oap.marshaller;

import com.alibaba.fastjson.JSON;
import com.jprass.oap.Constants;
import com.jprass.oap.RopException;
import com.jprass.oap.RopMarshaller;

import java.io.IOException;
import java.io.OutputStream;

/**
 * <pre>
 *    将响应对象流化成JSON。 {@link ObjectMapper}是线程安全的。
 * </pre>
 *
 * @version 1.0
 */
public class FastJsonRopMarshaller implements RopMarshaller {

	public void marshaller(Object object, OutputStream outputStream) {
		try {
			outputStream.write(JSON.toJSONString(object).getBytes(Constants.UTF8));
		} catch (IOException e) {
			throw new RopException(e);
		}
	}

}
