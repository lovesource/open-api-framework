package com.jprass.oap.event;

import java.util.EventObject;

import com.jprass.oap.RopContext;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @version 1.0
 */
public abstract class OapEvent extends EventObject {

	private static final long serialVersionUID = -7593225595321267149L;
	
	private RopContext ropContext;

	public OapEvent(Object source, RopContext ropContext) {
		super(source);
		this.ropContext = ropContext;
	}

	public RopContext getRopContext() {
		return ropContext;
	}
}
