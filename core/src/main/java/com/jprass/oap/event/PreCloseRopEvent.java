package com.jprass.oap.event;

import com.jprass.oap.RopContext;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @version 1.0
 */
public class PreCloseRopEvent extends OapEvent {
	private static final long serialVersionUID = 311817518116424174L;

	public PreCloseRopEvent(Object source, RopContext ropContext) {
        super(source, ropContext);
    }
}

