package com.jprass.oap.event;

import java.util.EventListener;

/**
 * <pre>
 *    监听所有Oap框架的事件
 * </pre>
 *
 * @version 1.0
 */
public interface RopEventListener<E extends OapEvent> extends EventListener {

    /**
     * 响应事件
     *
     * @param ropEvent
     */
    void onRopEvent(E ropEvent);

    /**
     * 执行的顺序号
     *
     * @return
     */
    int getOrder();
}

