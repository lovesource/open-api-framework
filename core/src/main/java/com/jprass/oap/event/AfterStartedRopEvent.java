package com.jprass.oap.event;

import com.jprass.oap.RopContext;

/**
 * <pre>
 *   在Oap框架初始化后产生的事件
 * </pre>
 *
 * @version 1.0
 */
public class AfterStartedRopEvent extends OapEvent {

	private static final long serialVersionUID = 1L;

	public AfterStartedRopEvent(Object source, RopContext ropContext) {
        super(source, ropContext);
    }

}

