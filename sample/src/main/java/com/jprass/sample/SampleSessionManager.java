/**
 * 版权声明：中图一购网络科技有限公司 版权所有 违者必究 2012 
 * 日    期：12-7-17
 */
package com.jprass.sample;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.jprass.oap.session.Session;
import com.jprass.oap.session.SessionManager;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @author 陈雄华
 * @version 1.0
 */
public class SampleSessionManager implements SessionManager {
	protected final Logger logger = Logger.getLogger(getClass());

	private final Map<String, Session> sessionCache = new ConcurrentHashMap<String, Session>(128, 0.75f, 32);

	public void addSession(String sessionId, Session session) {
		sessionCache.put(sessionId, session);
	}

	public Session getSession(String sessionId) {
		return sessionCache.get(sessionId);
	}

	public void removeSession(String sessionId) {
		sessionCache.remove(sessionId);
	}
}
