package com.jprass.sample.api;

import com.jprass.oap.annotation.NeedInSessionType;
import com.jprass.oap.annotation.ServiceMethod;
import com.jprass.oap.annotation.ServiceMethodBean;
import com.jprass.oap.response.OapResponse;
import com.jprass.sample.request.LogonRequest;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @version 1.0
 */
@ServiceMethodBean(version = "1.0")
public class UserApi {

	@ServiceMethod(method = "user.logon", version = "1.0", needInSession = NeedInSessionType.NO)
	public OapResponse<String> logon(LogonRequest request) {
		return new OapResponse<String>().setBody("OK");
	}

}
