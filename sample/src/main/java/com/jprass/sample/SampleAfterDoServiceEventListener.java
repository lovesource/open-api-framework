/**
 * 版权声明：中图一购网络科技有限公司 版权所有 违者必究 2012 
 * 日    期：12-6-2
 */
package com.jprass.sample;

import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.jprass.oap.RopRequestContext;
import com.jprass.oap.event.AfterDoServiceEvent;
import com.jprass.oap.event.RopEventListener;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @author 陈雄华
 * @version 1.0
 */
public class SampleAfterDoServiceEventListener implements RopEventListener<AfterDoServiceEvent> {

	public void onRopEvent(AfterDoServiceEvent ropEvent) {
		RopRequestContext ropRequestContext = ropEvent.getRopRequestContext();
		if (ropRequestContext != null) {
			Map<String, String> allParams = ropRequestContext.getAllParams();
			System.out.println(JSON.toJSON(allParams));
		}
	}

	public int getOrder() {
		return 0;
	}
}
