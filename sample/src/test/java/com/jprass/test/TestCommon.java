package com.jprass.test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class TestCommon {
	public static void main(String[] args) {
		Map<String, String> form = new LinkedHashMap<String, String>(16);
		// 系统级参数
		form.put("appKey", "00001");
		form.put("method", "user.logon");
		form.put("version", "1.0");
		// 业务级参数
//		form.put("userName", "stjdydayou");

		// 对请求进行签名
		String signValue = TestCommon.sign(form, "abcdeabcdeabcdeabcdeabcde");

//		form.put("password", "123456");
		form.put("sign", signValue);

		System.out.println(TestCommon.post("http://127.0.0.1:8080/router", form));

	}

	public static String sign(Map<String, String> paramValues, String secret) {
		StringBuilder sign = new StringBuilder();

		try {
			byte[] sha1Digest = null;
			StringBuilder sb = new StringBuilder();
			List<String> paramNames = new ArrayList<String>(paramValues.size());
			paramNames.addAll(paramValues.keySet());
			Collections.sort(paramNames);

			sb.append(secret);
			for (String paramName : paramNames) {
				sb.append(paramName).append(paramValues.get(paramName));
			}
			sb.append(secret);
			System.out.println(sb.toString());
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			sha1Digest = md.digest(sb.toString().getBytes("UTF-8"));

			for (int i = 0; i < sha1Digest.length; i++) {
				String hex = Integer.toHexString(sha1Digest[i] & 0xFF);
				if (hex.length() == 1) {
					sign.append("0");
				}
				sign.append(hex.toUpperCase());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sign.toString();
	}

	public static String post(String url, Map<String, String> params) {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpPost post = postForm(url, params);
		HttpResponse response = sendRequest(httpclient, post);
		String body = paseResponse(response);

		httpclient.getConnectionManager().shutdown();

		return body;
	}

	public static String get(String url, Map<String, String> params) {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet get = getForm(url, params);
		HttpResponse response = sendRequest(httpclient, get);
		String body = paseResponse(response);

		httpclient.getConnectionManager().shutdown();

		return body;
	}

	private static HttpGet getForm(String url, Map<String, String> params) {

		StringBuffer urlParams = new StringBuffer();
		Set<String> keySet = params.keySet();
		for (String key : keySet) {
			urlParams.append(key).append("=").append(params.get(key)).append("&");
		}
		url = url + "?" + urlParams.substring(0, urlParams.length() - 1);
		HttpGet get = new HttpGet(url);
		return get;
	}

	private static HttpPost postForm(String url, Map<String, String> params) {

		HttpPost httpost = new HttpPost(url);
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();

		Set<String> keySet = params.keySet();
		for (String key : keySet) {
			nvps.add(new BasicNameValuePair(key, params.get(key)));
		}

		try {
			httpost.setEntity(new UrlEncodedFormEntity(nvps));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return httpost;
	}

	private static HttpResponse sendRequest(DefaultHttpClient httpclient, HttpUriRequest httpost) {
		HttpResponse response = null;
		try {
			response = httpclient.execute(httpost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	private static String paseResponse(HttpResponse response) {
		String body = null;
		try {
			if (response != null && response.getStatusLine().getStatusCode() == 200) {
				HttpEntity entity = response.getEntity();
				body = EntityUtils.toString(entity);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return body;
	}
}
